package com.example.job.discover.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author magj
 * @since 2019-08-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="DiscoverMaxHot对象", description="每日热点奖励金币实体")
public class DiscoverMaxHot implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "发现用户表ID")
    private Long id;

    @ApiModelProperty(value = "用户ID")
    private String userPid;




}
