package com.example.job.discover.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author magj
 * @since 2019-08-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("discover_user")
@ApiModel(value="DiscoverUser对象", description="")
public class DiscoverUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "发现用户表")
    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long id;

    @ApiModelProperty(value = "状态：1正常，0已取消")
    private String status;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "头像")
    private String avatar;

    @ApiModelProperty(value = "是否认证（认证状态 0未认证 1认证蓝V 2认证红V 3认证失败）")
    private String comfirmState;

    @ApiModelProperty(value = "省份ID")
    private String cityId;

    @ApiModelProperty(value = "省份名称")
    private String city;

    @ApiModelProperty(value = "学校编码")
    private String collegeId;

    @ApiModelProperty(value = "学校名称")
    private String collegeName;

    @ApiModelProperty(value = "学院名称")
    private String academyName;

    @ApiModelProperty(value = "描述")
    private String remark;


}
