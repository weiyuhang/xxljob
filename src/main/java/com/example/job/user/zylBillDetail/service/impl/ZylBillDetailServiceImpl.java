package com.example.job.user.zylBillDetail.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.job.mapper.ZylBillDetailMapper;
import com.example.job.user.zylBillDetail.entity.ZylBillDetail;
import com.example.job.user.zylBillDetail.service.IZylBillDetailService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 金币流水表 服务实现类
 * </p>
 *
 * @author weiyuhang
 * @since 2019-08-30
 */
@Service
public class ZylBillDetailServiceImpl extends ServiceImpl<ZylBillDetailMapper, ZylBillDetail> implements IZylBillDetailService {

}
