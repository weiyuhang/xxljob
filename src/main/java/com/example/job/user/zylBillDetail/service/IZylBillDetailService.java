package com.example.job.user.zylBillDetail.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.job.user.zylBillDetail.entity.ZylBillDetail;

/**
 * <p>
 * 金币流水表 服务类
 * </p>
 *
 * @author weiyuhang
 * @since 2019-08-30
 */
public interface IZylBillDetailService extends IService<ZylBillDetail> {

}
