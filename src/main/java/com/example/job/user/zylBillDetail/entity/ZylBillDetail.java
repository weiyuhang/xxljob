package com.example.job.user.zylBillDetail.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 金币流水表
 * </p>
 *
 * @author weiyuhang
 * @since 2019-08-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ZylBillDetail对象", description="金币流水表")
public class ZylBillDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "bill_detail_id", type = IdType.ID_WORKER)
    @ApiModelProperty(value = "主键")
    private Long billDetailId;

    @ApiModelProperty(value = "图片路径")
    private String headImg;

    @ApiModelProperty(value = "商家名称")
    private String name;

    @ApiModelProperty(value = "交易类型0商城 1悬赏 2投票 3发现 4课程表 5评论 6建群 7发红包 8抢红包 9退红包 10排行榜11二手市场")
    private Integer dealType;

    @ApiModelProperty(value = "交易时间")
    private Date dealTime;

    @ApiModelProperty(value = "交易金额")
    private Long dealMoney;

    @ApiModelProperty(value = "关联的用户id")
    private Long userId;

    @ApiModelProperty(value = "支出还是收入 0支出 1 收入")
    private Integer inOrOut;


}
