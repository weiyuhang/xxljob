package com.example.job.user.goldMoney.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 金币表
 * </p>
 *
 * @author weiyuhang
 * @since 2019-08-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ZylGoldMoney对象", description="金币表")
public class ZylGoldMoney implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "gold_money_id", type = IdType.AUTO)
    private Long goldMoneyId;

    @ApiModelProperty(value = "金币数量")
    private Long goldMoneyNum;

    @ApiModelProperty(value = "关联的用户id")
    private Long userId;


}
