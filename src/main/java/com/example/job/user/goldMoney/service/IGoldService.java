package com.example.job.user.goldMoney.service;


public interface IGoldService {

    /**
     * 金币消费通用服务
     * @param goldNum 金币数
     * @param inOrOut 收入，支出
     * @param userId 用户ID
     * @param dealType 交易类型
     * @param billName 交易描述
     */
    public void goldGetOrConsume(String headImgUrl, Integer goldNum, Integer inOrOut, Long userId, Integer dealType, String billName);
}
