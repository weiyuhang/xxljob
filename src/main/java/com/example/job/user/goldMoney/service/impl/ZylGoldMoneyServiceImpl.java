package com.example.job.user.goldMoney.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.job.user.goldMoney.entity.ZylGoldMoney;
import com.example.job.user.goldMoney.service.IZylGoldMoneyService;
import com.example.job.mapper.ZylGoldMoneyMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 金币表 服务实现类
 * </p>
 *
 * @author weiyuhang
 * @since 2019-08-30
 */
@Service
public class ZylGoldMoneyServiceImpl extends ServiceImpl<ZylGoldMoneyMapper, ZylGoldMoney> implements IZylGoldMoneyService {

}
