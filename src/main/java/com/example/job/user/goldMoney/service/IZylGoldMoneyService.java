package com.example.job.user.goldMoney.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.job.user.goldMoney.entity.ZylGoldMoney;

/**
 * <p>
 * 金币表 服务类
 * </p>
 *
 * @author weiyuhang
 * @since 2019-08-30
 */
public interface IZylGoldMoneyService extends IService<ZylGoldMoney> {

}
