package com.example.job.user.goldMoney.service.impl;

import cn.hutool.core.date.DateTime;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.job.im.dict.service.IDictService;
import com.example.job.mapper.ZylBillDetailMapper;
import com.example.job.mapper.ZylBillExtMapper;
import com.example.job.mapper.ZylGoldMoneyMapper;
import com.example.job.user.goldMoney.entity.ZylGoldMoney;
import com.example.job.user.goldMoney.service.IGoldService;
import com.example.job.user.zylBillDetail.entity.ZylBillDetail;
import com.example.job.user.zylBillExt.entity.ZylBillExt;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.Date;

@Service
@Slf4j
public class GoldService implements IGoldService {

//    @Resource
//    private BaseUserService baseUserService;
    @Resource
    private IDictService dictService;
    @Resource
    private ZylGoldMoneyMapper goldMoneyMapper;
    @Resource
    private ZylBillDetailMapper billDetailMapper;
    @Resource
    private ZylBillExtMapper billExtMapper;



    @Override
    // 开启新线程运行
    @Async
    public void goldGetOrConsume(String headImgUrl,Integer goldNum, Integer inOrOut, Long userId, Integer dealType, String billName){
        //GoldMoney金币表
        //1.判断金币是否够用
        ZylBillDetail billDetail = new ZylBillDetail();
        billDetail.setInOrOut(inOrOut);
        billDetail.setUserId(userId);
        billDetail.setHeadImg(headImgUrl);
        billDetail.setDealType(dealType);
        billDetail.setDealMoney(goldNum.longValue());
        billDetail.setDealTime(new Date());
        billDetail.setName(billName);
        QueryWrapper<ZylGoldMoney> queryGoldMoney = new QueryWrapper<>();
        queryGoldMoney.eq("user_id",billDetail.getUserId());
        ZylGoldMoney goldMoney = goldMoneyMapper.selectOne(queryGoldMoney);
        if(StringUtils.isEmpty(goldMoney)){
            goldMoney = new ZylGoldMoney();
            goldMoney.setGoldMoneyNum(0L);
            goldMoney.setUserId(billDetail.getUserId());
        }
        if(goldMoney.getGoldMoneyNum()+billDetail.getDealMoney()<0){
            throw new RuntimeException("金币不足，不能进行该操作");
        }else{
            //金币充足
            goldMoney.setGoldMoneyNum(goldMoney.getGoldMoneyNum()+billDetail.getDealMoney());
            //更新金币记录表
            if(goldMoney.getGoldMoneyId()==null){
                goldMoneyMapper.insert(goldMoney);
            }else{
                goldMoneyMapper.updateById(goldMoney);
            }
            //保存消费明细表
            if(!StringUtils.isEmpty(billDetail.getBillDetailId())){
                billDetail.setBillDetailId(null);
            }
            billDetailMapper.insert(billDetail);
            //修改每月总收支明细表
            //1. 设置查询条件
            QueryWrapper<ZylBillExt> queryBillExt = new QueryWrapper<>();
            queryBillExt.eq("user_id",billDetail.getUserId());
            queryBillExt.eq("which_year_which_month",getDateStr(billDetail.getDealTime()));
            //2.查询当前时间 的每月总收支明细表 记录
            ZylBillExt billExt = billExtMapper.selectOne(queryBillExt);
            if(StringUtils.isEmpty(billExt)){
                //月初第一单交易
                //创建BillExt
                billExt = new ZylBillExt();
                billExt.setUserId(billDetail.getUserId());//设置id
                //设置支出/收入金额
                if(billDetail.getInOrOut()==0){
                    billExt.setCountPay(billDetail.getDealMoney());
                }else{
                    billExt.setCountIn(billDetail.getDealMoney());
                }
                //设置时间
                //初始化BillExt

                billExt.setWhichYearWhichMonth(getDateStr(billDetail.getDealTime()));
                //保存每月总收支明细表
                billExtMapper.insert(billExt);
            }else{
                //非
                if(billDetail.getInOrOut()==0){
                    //支出  保存到当月的支出集合
                    billExt.setCountPay(billExt.getCountPay()-billDetail.getDealMoney());
                }else{
                    //收入 保存到当月的收入集合
                    billExt.setCountIn((billExt.getCountIn()+billDetail.getDealMoney()));
                }
                //更新每月总收支明细表
                billExtMapper.updateById(billExt);
            }
        }
    }

    public String getDateStr(Date date){
        DateTime time = new DateTime(date);
        System.out.println(time);

        //对时间处理
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int mouth = calendar.get(Calendar.MONTH)+1;
        System.out.println(mouth);
        return year+"-0"+mouth;

    }

}
