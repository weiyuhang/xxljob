package com.example.job.user.zylBillExt.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.job.mapper.ZylBillExtMapper;
import com.example.job.user.zylBillExt.entity.ZylBillExt;
import com.example.job.user.zylBillExt.service.IZylBillExtService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 金币每月收支统计表 服务实现类
 * </p>
 *
 * @author weiyuhang
 * @since 2019-08-30
 */
@Service
public class ZylBillExtServiceImpl extends ServiceImpl<ZylBillExtMapper, ZylBillExt> implements IZylBillExtService {

}
