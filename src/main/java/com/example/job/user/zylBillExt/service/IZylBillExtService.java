package com.example.job.user.zylBillExt.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.job.user.zylBillExt.entity.ZylBillExt;

/**
 * <p>
 * 金币每月收支统计表 服务类
 * </p>
 *
 * @author weiyuhang
 * @since 2019-08-30
 */
public interface IZylBillExtService extends IService<ZylBillExt> {

}
