package com.example.job.user.zylBillExt.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 金币每月收支统计表
 * </p>
 *
 * @author weiyuhang
 * @since 2019-08-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ZylBillExt对象", description="金币每月收支统计表")
public class ZylBillExt implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "bill_ext_id", type = IdType.AUTO)
    private Long billExtId;

    @ApiModelProperty(value = "那一年那一月记录如：199802")
    private String whichYearWhichMonth;

    @ApiModelProperty(value = "总支出")
    private Long countPay;

    @ApiModelProperty(value = "总收入")
    private Long countIn;

    @ApiModelProperty(value = "关联用户id")
    private Long userId;


}
