package com.example.job.service;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.job.exception.BusiException;
import com.example.job.im.activityGroup.entity.ActivityGroup;
import com.example.job.im.activityGroup.service.IActivityGroupService;
import com.example.job.im.dict.service.IDictService;
import com.example.job.im.group.entity.Group;
import com.example.job.im.historyMessage.HistroyMessage;
import com.example.job.mapper.GroupMapper;
import com.example.job.im.groupUser.entity.GroupUser;
import com.example.job.mapper.GroupUserMapper;
import com.example.job.user.goldMoney.service.IGoldService;
import com.example.job.util.FindJsonUtil;
import com.example.job.util.GoldStaticConfig;
import com.example.job.util.GroupStaticConfig;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.zyl.core.enums.ResultCode;
import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.expression.StringValue;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import com.example.job.util.RongCloutHttpRequestUtil;
import javax.annotation.Resource;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@JobHandler("hotGroupSortJob")
@Component
@Slf4j
public class HotGroupSortJob extends IJobHandler {
    private static final int BUFFER_SIZE = 10 * 1024;
    @Resource
    private RongCloutHttpRequestUtil rongCloutHttpRequest;
    @Resource
    private IActivityGroupService activityGroupService;
    @Resource
    private GroupUserMapper groupUserMapper;
    @Resource
    private GroupMapper groupMapper;
    @Resource
    private IGoldService goldService;
    @Resource
    private IDictService dictService;

    private static final String DOWNLOAD_MSG_HISTORY_METHOD = "message/history";

    @Override
    public ReturnT<String> execute(String s) throws Exception {
        log.error("群组历史聊天记录分析热点群组JOB启动HotGroupSortJob=========");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        Date date = calendar.getTime();
        calendar.add(Calendar.HOUR, -2);
        List<Map<String, Object>> maps = new ArrayList<>();
        // 1.拿到融云过去24小时数据下载地址
        for (int i = 0; i < 24; i++) {
            String yesterday = new SimpleDateFormat("yyyyMMddHH").format(calendar.getTime());
            Map parammap = new HashMap<>();
            parammap.put("date", yesterday);
            Map<String, Object> resultMap = rongCloutHttpRequest.doPost(DOWNLOAD_MSG_HISTORY_METHOD, parammap);
            calendar.add(Calendar.HOUR, 1);
            if (StringUtils.isBlank(resultMap.get("url").toString())) {
                continue;
            }
            Map<String,Object> map1 = new HashMap<>();
            map1.put(yesterday,resultMap);
            maps.add(map1);
        }
        Map<String, Integer> map = new HashMap<>();
        for(Map<String,Object> map1 : maps){
            for(String key : map1.keySet()){
                String yesterday = key;
                Map<String,Object> resultMap = (Map<String,Object>)map1.get(key);
                // 2.下载聊天记录zip
                String rootDir = downImMsg(yesterday, resultMap);
                // 3.解压文件 读取聊天记录文件
                List<String> filePath = entryZipFile(rootDir);
                // 4.读取文件数据转换为json等待分析
                List<String> jsonListList = saveDBForReadFileData(filePath);
                // 5.归纳各小时聊天数据
                map = analyzeHotGroupData(jsonListList,map);
            }
        }
        calendar.add(Calendar.HOUR,1);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.MILLISECOND,0);
        Date createTime = calendar.getTime();
        //6.分析数据并存入数据库
        if (!map.isEmpty()) {
            // 6.整理所有数据并按照同城校内热点排序
            sortCollegeOrCityGroup(map, createTime);
        }
        // 7.删除文件夹里的所有文件
        delFolder("//msgfile");
        log.error("群组历史聊天记录分析热点群组JOB执行完成ReturnGoldJob=========");

        return SUCCESS;
    }
    private boolean  delAllFile(String folderPath) {
        boolean flag = false;
        File file = new File(folderPath);
        if (!file.exists()) {
            return flag;
        }
        if (!file.isDirectory()) {
            return flag;
        }
        String[] tempList = file.list();
        File temp = null;
        for (int i = 0; i < tempList.length; i++) {
            if (folderPath.endsWith(File.separator)) {
                temp = new File(folderPath + tempList[i]);
            } else {
                temp = new File(folderPath + File.separator + tempList[i]);
            }
            if (temp.isFile()) {
                temp.delete();
            }
            if (temp.isDirectory()) {
                delAllFile(folderPath + "//" + tempList[i]);//先删除文件夹里面的文件
                delFolder(folderPath + "//" + tempList[i]);//再删除空文件夹
                flag = true;
            }
        }
        return flag;

    }

    public  void delFolder(String folderPath) {
        try {
            delAllFile(folderPath); //删除完里面所有内容
            String filePath = folderPath;
            filePath = filePath.toString();
            java.io.File myFilePath = new java.io.File(filePath);
            myFilePath.delete(); //删除空文件夹
        } catch (Exception e) {
            log.error("删除文件失败======================" + e.getMessage());
            throw new BusiException(ResultCode.RESULT_FAILURE.getCode() + "", "删除文件失败");
        }
    }

    private void sortCollegeOrCityGroup(Map<String, Integer> map, Date createTime) throws Exception{
//        activityGroupService.removeById(null);
        Map paramMap = new HashMap();
        paramMap.put("createTime",createTime);
        List<Map<String,Object>> list = groupUserMapper.countGroupUser(paramMap);
        for (Map<String,Object> listMap : list) {
            System.out.println("当前群组:" + JSON.toJSONString(listMap));
            for(String key : map.keySet()){
                if(String.valueOf(listMap.get("groupId")).equals(key)){
                    listMap.put("groupUserCount",String.valueOf(map.get(key)+Integer.parseInt(listMap.get("groupUserCount").toString())));
                    break;
                }
            }
            //把整理好的昨天的数据存入数据库
            ActivityGroup activityGroup = new ActivityGroup();
            activityGroup.setActivity(Integer.parseInt(listMap.get("groupUserCount").toString()));
            activityGroup.setGroupId(Long.parseLong(listMap.get("groupId").toString()));
            activityGroup.setActivityDate(createTime);
            activityGroup.setCreateTime(new Date());
            activityGroupService.save(activityGroup);
        }
        List<Long> collegeList = groupMapper.collegeGroupByList(paramMap);
        List<Map<String,Object>> collegeGroupList = new ArrayList<>();
        List<Map<String,Object>> cityGroupList = new ArrayList<>();
        List<Map<String,Object>> nationGroupList = new ArrayList<>();
        List<Long> cityList = groupMapper.cityGroupByList(paramMap);
        List<Long> nationList = groupMapper.nationGroupByList(paramMap);

        //分开校内群和同城群
        for (Map<String,Object> listMap : list) {
            Group group = groupMapper.selectById(Long.parseLong(listMap.get("groupId").toString()));
            if(GroupStaticConfig.Attribute.SAME_UNIVERSITY.equals(group.getAttribute().toString())){
                collegeGroupList.add(listMap);
            }else if(GroupStaticConfig.Attribute.SAME_CITY.equals(group.getAttribute().toString())){
                cityGroupList.add(listMap);
            }else if(GroupStaticConfig.Attribute.SAME_NATION.equals(group.getAttribute().toString())){
                nationGroupList.add(listMap);
            }
        }
        for(long collegeId : collegeList){
            List<Map<String,Object>> mapList = new ArrayList<>();
            for(Map<String,Object> collegeGroupMap :collegeGroupList){
                Group group = groupMapper.selectById(Long.parseLong(collegeGroupMap.get("groupId").toString()));
                if(group.getCollegeId().equals(collegeId)){
                    mapList.add(collegeGroupMap);
                }
            }
            //排序并发奖励
            sortGrouMap(mapList,"_COLLEGE_HOT_GROUP_GOLD");
        }
        for(long cityId : cityList){
            List<Map<String,Object>> mapList = new ArrayList<>();
            for(Map<String,Object> cityGroupMap :cityGroupList){
                Group group = groupMapper.selectById((Long)cityGroupMap.get("groupId"));
                if(group.getCityId().equals(cityId)){
                    mapList.add(cityGroupMap);
                }
            }
            //排序并发奖励
            sortGrouMap(mapList,"_CITY_HOT_GROUP_GOLD");
        }

        for(long nationId : nationList){
            List<Map<String,Object>> mapList = new ArrayList<>();
            for(Map<String,Object> nationGroupMap :nationGroupList){
                Group group = groupMapper.selectById((Long)nationGroupMap.get("groupId"));
                if(group.getNationId().equals(nationId)){
                    mapList.add(nationGroupMap);
                }
            }
            //排序并发奖励
            sortGrouMap(mapList,"_NATION_HOT_GROUP_GOLD");
        }
    }

    private void sortGrouMap(List<Map<String, Object>> mapList ,String dictName) {
        //排序
        Collections.sort(mapList, new Comparator<Map<String, Object>>() {
            @Override
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                Integer integer1 = Integer.parseInt(o1.get("groupUserCount").toString());
                Integer integer2 = Integer.parseInt(o2.get("groupUserCount").toString());
                if (integer1 > integer2) {
                    return -1;
                }
                if (integer1.equals(integer2)) {
                    return 0;
                }
                return 1;
            }
        });
        String firstGold = dictService.getCodeValue("FIRST"+dictName);
        String secondGold = dictService.getCodeValue("SECOND"+dictName);
        String thirdGold = dictService.getCodeValue("THIRD"+dictName);
        getGoldForHotGroup(mapList.size()>0?mapList.get(0):null,firstGold);
        getGoldForHotGroup(mapList.size()>1?mapList.get(1):null,secondGold);
        getGoldForHotGroup(mapList.size()>2?mapList.get(2):null,thirdGold);
    }

    private void getGoldForHotGroup(Map<String, Object> map, String firstGold) {
        if(null != map){
            QueryWrapper<GroupUser> groupUserQueryWrapper = new QueryWrapper<>();
            groupUserQueryWrapper.eq("group_id",map.get("groupId"));
            groupUserQueryWrapper.eq("group_user_type",GroupStaticConfig.GroupUserType.GROUP_CREATOR);
            GroupUser groupUser = groupUserMapper.selectOne(groupUserQueryWrapper);
            if(StringUtils.isNotBlank(firstGold)){
                String billName = "吐槽群热度奖励";
                // 头像链接
                String groupHeadImgUrl = dictService.getCodeValue("GROUP_HEAD_IMG_URL");//从字典表中读取配置
                goldService.goldGetOrConsume(groupHeadImgUrl,Integer.parseInt(firstGold), Integer.parseInt(GoldStaticConfig.ConsumeOrGet.GET),
                        groupUser.getUserId(),Integer.parseInt(GoldStaticConfig.DealType.CREATE_GROUP),billName);
            }
        }

    }


    private Map<String, Integer>  analyzeHotGroupData(List<String> jsonListList, Map<String, Integer> mapCount) throws BusiException {
        try {
            for (String json : jsonListList) {
                HistroyMessage histroyMessage = JSON.parseObject(json, HistroyMessage.class);
                if (null != histroyMessage && StringUtils.isNotBlank(histroyMessage.getGroupId())) {
                    if (!mapCount.isEmpty() && mapCount.containsKey(histroyMessage.getGroupId())) {
                        mapCount.put(histroyMessage.getGroupId(), mapCount.get(histroyMessage.getGroupId()) + 1);
                        continue;
                    }
                    mapCount.put(histroyMessage.getGroupId(), 1);
                }
            }
            return mapCount;

        } catch (Exception e) {
            log.error("分析json数据失败======================" + e.getMessage());
            throw new BusiException(ResultCode.RESULT_FAILURE.getCode() + "", "操作失败");
        }
    }


    private List<String> saveDBForReadFileData(List<String> filePath) throws BusiException {
        List<String> jsonListList = new ArrayList<>();
        BufferedReader reader = null;
        try {
            for (String fileName : filePath) {
                StringBuffer sbf = new StringBuffer();
                File file = new File(fileName);
                reader = new BufferedReader(new FileReader(file));
                String tempStr;
                while ((tempStr = reader.readLine()) != null) {
                    sbf.append(tempStr);
                }
                List<String> list = FindJsonUtil.format(sbf.toString());
                jsonListList.addAll(list);
                reader.close();
            }
        } catch (Exception e) {
            log.error("读取信息文件数据失败{" + filePath + "}======================" + e.getMessage());
            throw new BusiException(ResultCode.RESULT_FAILURE.getCode() + "", "操作失败");
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e1) {
                    log.error("读取信息文件文件流关闭失败{" + filePath + "}======================" + e1.getMessage());
                    throw new BusiException(ResultCode.RESULT_FAILURE.getCode() + "", "操作失败");
                }
            }
            return jsonListList;
        }
    }

    private List<String> entryZipFile(String rootDir) {
        List<String> filePath = new ArrayList<>();
        File file = FileUtil.file(rootDir);
        if (!file.exists()) {
            throw new RuntimeException(file.getPath() + "所指文件不存在");
        }
        // 开始解压
        ZipFile zipFile = null;
        try {
            zipFile = new ZipFile(file);
            Enumeration<?> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) entries.nextElement();
                System.out.println("解压" + entry.getName());
                // 如果是文件夹，就创建个文件夹
                if (entry.isDirectory()) {
                    String dirPath = "//msgfile//" + entry.getName();
                    File dir = new File(dirPath);
                    dir.mkdirs();
                } else {
                    // 如果是文件，就先创建一个文件，然后用io流把内容copy过去
                    File targetFile = new File("//msgfile//" + entry.getName());
                    filePath.add("//msgfile//" + entry.getName());
                    // 保证这个文件的父文件夹必须要存在
                    if (!targetFile.getParentFile().exists()) {
                        targetFile.getParentFile().mkdirs();
                    }
                    targetFile.createNewFile();
                    // 将压缩文件内容写入到这个文件中
                    InputStream is = zipFile.getInputStream(entry);
                    FileOutputStream fos = new FileOutputStream(targetFile);
                    int len;
                    byte[] buf = new byte[BUFFER_SIZE];
                    while ((len = is.read(buf)) != -1) {
                        fos.write(buf, 0, len);
                    }
                    // 关流顺序，先打开的后关闭
                    fos.close();
                    is.close();
                }
            }
            return filePath;
        } catch (Exception e) {
            log.error("解压文件失败{" + rootDir + "}======================" + e.getMessage());
            throw new BusiException(ResultCode.RESULT_FAILURE.getCode() + "", "操作失败");
        } finally {
            if (zipFile != null) {
                try {
                    zipFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String downImMsg(String yesterday, Map<String, Object> map) throws BusiException {
        String rootDir = "";
        String urlStr = map.get("url").toString();
        try {
            URL url = new URL(urlStr);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //设置超时间为3秒
            conn.setConnectTimeout(3 * 1000);
            conn.setReadTimeout(1000 * 1000);
            //防止屏蔽程序抓取而返回403错误
            conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
            //得到输入流
            InputStream inputStream = conn.getInputStream();
            //获取自己数组
            byte[] getData = readInputStream(inputStream);

            //文件保存位置
            File saveDir = new File("//msgfile");
            if (!saveDir.exists()) {
                saveDir.mkdir();
            }
            File file = new File(saveDir + File.separator + yesterday + ".zip");
            rootDir = file.getCanonicalPath();
            System.out.println("当前工程所在文件夹：" + rootDir);
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(getData);
            if (fos != null) {
                fos.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }

            System.out.println("info:" + url + " download success");
            return rootDir;
        } catch (Exception e) {
            log.error("下载聊天信息失败{" + urlStr + "}===============================" + e.getMessage());
            throw new BusiException(ResultCode.RESULT_FAILURE.getCode() + "", "操作失败");
        }
    }

    /**
     * 从输入流中获取字节数组
     *
     * @param inputStream
     * @return
     * @throws IOException
     */
    public static byte[] readInputStream(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[1024];
        int len = 0;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while ((len = inputStream.read(buffer)) != -1) {
            bos.write(buffer, 0, len);
        }
        bos.close();
        return bos.toByteArray();
    }
}
