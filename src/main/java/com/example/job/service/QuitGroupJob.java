package com.example.job.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.job.im.group.entity.GroupParam;
import com.example.job.im.group.entity.UserParam;
import com.example.job.im.group.service.IGroupService;
import com.example.job.im.groupUser.entity.GroupUser;
import com.example.job.im.groupUser.service.IGroupUserService;
import com.example.job.mapper.ActivityGroupMapper;
import com.example.job.util.GroupStaticConfig;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;

@JobHandler("quitGroupJob")
@Component
@Slf4j
public class QuitGroupJob extends IJobHandler {

    @Resource
    private ActivityGroupMapper activityGroupMapper;
    @Resource
    private IGroupService groupService;
    @Resource
    private IGroupUserService groupUserService;
    @Override
    public ReturnT<String> execute(String s) throws Exception {
        log.error("退群执行启动quitGroupJob============");
        //三天ActivityGroup表群聊天记录都是0的话就删除群
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date now = calendar.getTime();

        calendar.add(Calendar.DATE,-3);
        calendar.add(Calendar.SECOND,1);
        Date threeDays = calendar.getTime();
        Map<String ,Object> paramMap = new HashMap<>();
        paramMap.put("startTime",threeDays);
        paramMap.put("endTime",now);
        List<Map<String,String>> groupList = activityGroupMapper.countGroupMessage(paramMap);
        if(null != groupList && !groupList.isEmpty()){
            for(Map<String,String> groupMap :groupList){
                Long groupId = Long.parseLong(String.valueOf(groupMap.get("groupId")));
                Integer sumActivity = Integer.parseInt(String.valueOf(groupMap.get("sumActivity")));
                //以现在群人数*2+10为基准 小于 即解散
                QueryWrapper<GroupUser> groupUserQueryWrapper = new QueryWrapper<>();
                groupUserQueryWrapper.eq("group_id",groupId);
                Integer groupUserCount = groupUserService.count(groupUserQueryWrapper);
                if(sumActivity < (groupUserCount*2+10)){
                    groupUserQueryWrapper.eq("group_user_type", Integer.parseInt(GroupStaticConfig.GroupUserType.GROUP_CREATOR));
                    GroupUser groupUser = groupUserService.getOne(groupUserQueryWrapper);
                    GroupParam groupParam = new GroupParam();
                    groupParam.setId(groupId);
                    groupParam.setUserParam(new UserParam().setId(groupUser.getUserId()));
                    groupService.dismissGroup(groupParam);
                }
            }
        }
        log.error("退群执行成功quitGroupJob=========");
        return SUCCESS;
    }


}
