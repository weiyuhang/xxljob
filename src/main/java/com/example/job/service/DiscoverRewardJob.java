package com.example.job.service;

import com.example.job.discover.entity.DiscoverMaxHot;
import com.example.job.im.dict.service.IDictService;
import com.example.job.mapper.DiscoverInfoMapper;
import com.example.job.mapper.DiscoverUserMapper;
import com.example.job.user.goldMoney.service.IGoldService;
import com.example.job.util.GoldStaticConfig;
import com.github.pagehelper.PageHelper;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@JobHandler("discoverRewardJob")
@Component
@Slf4j
public class DiscoverRewardJob extends IJobHandler {

    @Resource
    private DiscoverUserMapper userMapper;

    @Resource
    private IGoldService goldService;

    @Resource
    private DiscoverInfoMapper discoverInfoMapper;
    @Resource
    private IDictService dictService;

    @Override
    public ReturnT<String> execute(String s) throws Exception {
        log.error("discoverRewardJob（定时任务）开始=============================================");
        // 头像链接
        String discoverIcon = dictService.getCodeValue("DISCOVER_HEAD_IMG_URL");//从字典表中读取配置
        String billName = "发现热度奖励";
        // 校内发现奖励发放业务逻辑
        List<String> collegeIds = userMapper.queryAllCollege();
        for(String collegeId : collegeIds){
            PageHelper.startPage(1, 3, false);
            List<DiscoverMaxHot> discoverMaxHotList = discoverInfoMapper.selectMaxHotByCollege(collegeId, null,"1");
            for(int i = 0; i<discoverMaxHotList.size(); i++ ){
                if(i == 0){
                    String oneCampusGold = dictService.getCodeValue("NUM_ONE_PRIZE_CAMPUS_DISCOVER");//从字典表中读取配置
                    goldService.goldGetOrConsume(discoverIcon,Integer.parseInt(oneCampusGold),Integer.parseInt(GoldStaticConfig.ConsumeOrGet.GET),
                            Long.parseLong(discoverMaxHotList.get(0).getUserPid()) ,Integer.parseInt(GoldStaticConfig.DealType.HOT_DISCOVER), billName);
                }
                if(i == 1){
                    String twoCampusGold = dictService.getCodeValue("NUM_TWO_PRIZE_CAMPUS_DISCOVER");//从字典表中读取配置
                    goldService.goldGetOrConsume(discoverIcon,Integer.parseInt(twoCampusGold),Integer.parseInt(GoldStaticConfig.ConsumeOrGet.GET),
                            Long.parseLong(discoverMaxHotList.get(1).getUserPid()) ,Integer.parseInt(GoldStaticConfig.DealType.HOT_DISCOVER), billName);
                }
                if(i == 2){
                    String threeCampusGold = dictService.getCodeValue("NUM_THREE_PRIZE_CAMPUS_DISCOVER");//从字典表中读取配置
                    goldService.goldGetOrConsume(discoverIcon,Integer.parseInt(threeCampusGold),Integer.parseInt(GoldStaticConfig.ConsumeOrGet.GET),
                            Long.parseLong(discoverMaxHotList.get(2).getUserPid()) ,Integer.parseInt(GoldStaticConfig.DealType.HOT_DISCOVER), billName);
                }
            }
        }

        // 同城发现奖励发放业务逻辑
        List<String> cityIds = userMapper.queryAllCity();
        for(String cityId : cityIds){
            PageHelper.startPage(1, 3, false);
            List<DiscoverMaxHot> discoverMaxHots = discoverInfoMapper.selectMaxHotByCollege(null, cityId,"2");
            for(int i = 0; i<discoverMaxHots.size(); i++ ){
                if(i == 0){
                    String oneCityGold = dictService.getCodeValue("NUM_ONE_PRIZE_CITY_DISCOVER");//从字典表中读取配置
                    goldService.goldGetOrConsume(discoverIcon,Integer.parseInt(oneCityGold),Integer.parseInt(GoldStaticConfig.ConsumeOrGet.GET),
                            Long.parseLong(discoverMaxHots.get(0).getUserPid()) ,Integer.parseInt(GoldStaticConfig.DealType.HOT_DISCOVER), billName);
                }
                if(i == 1){
                    String twoCityGold = dictService.getCodeValue("NUM_TWO_PRIZE_CITY_DISCOVER");//从字典表中读取配置
                    goldService.goldGetOrConsume(discoverIcon,Integer.parseInt(twoCityGold),Integer.parseInt(GoldStaticConfig.ConsumeOrGet.GET),
                            Long.parseLong(discoverMaxHots.get(1).getUserPid()) ,Integer.parseInt(GoldStaticConfig.DealType.HOT_DISCOVER), billName);
                }
                if(i == 2){
                    String threeCityGold = dictService.getCodeValue("NUM_THREE_PRIZE_CITY_DISCOVER");//从字典表中读取配置
                    goldService.goldGetOrConsume(discoverIcon,Integer.parseInt(threeCityGold),Integer.parseInt(GoldStaticConfig.ConsumeOrGet.GET),
                            Long.parseLong(discoverMaxHots.get(2).getUserPid()) ,Integer.parseInt(GoldStaticConfig.DealType.HOT_DISCOVER), billName);
                }
            }
        }
        log.error("discoverRewardJob（定时任务）结束=============================================");
        return SUCCESS;
    }
}
