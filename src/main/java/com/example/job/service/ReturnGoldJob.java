package com.example.job.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.job.im.dict.service.IDictService;
import com.example.job.im.packets.entity.Packets;
import com.example.job.im.packets.service.IPacketsService;
import com.example.job.user.goldMoney.service.IGoldService;
import com.example.job.util.GoldStaticConfig;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@JobHandler("returnGoldJob")
@Component
@Slf4j
public class ReturnGoldJob extends IJobHandler {

    @Resource
    private IPacketsService packetsService;
    @Resource
    private IGoldService goldService;
    @Resource
    private IDictService dictService;
    @Override
    public ReturnT<String> execute(String s) throws Exception {
        log.error("扫描红包失效启动ReturnGoldJob=========");
        //查询超过24小时红包 让他失效 无法再抢
        QueryWrapper<Packets> queryWrapper = new QueryWrapper<>();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1); //得到前一天
        Date yesterday = calendar.getTime();
        queryWrapper.le("create_time",yesterday);
        queryWrapper.ne("stock",0);
        queryWrapper.eq("status", 0);
        List<Packets> packetsList = packetsService.list(queryWrapper);
        for(Packets packets :packetsList){
            packets.setStatus(1);
            packets.setUpdateTime(new Date());
            packetsService.updateById(packets);
            //然后退钱
            Integer goldNum = packets.getStock()*packets.getAmount();
            String billName = "红包退回";
            // 头像链接
            String packetsHeadImgUrl = dictService.getCodeValue("PACKETS_HEAD_IMG_URL");//从字典表中读取配置
            goldService.goldGetOrConsume(packetsHeadImgUrl,goldNum,Integer.parseInt(GoldStaticConfig.ConsumeOrGet.GET),
                    packets.getRecUserId(),Integer.parseInt(GoldStaticConfig.DealType.GET_RED_PACKETS),billName);
        }
        log.error("扫描红包失效执行完成ReturnGoldJob=========");
        return SUCCESS;
    }
}
