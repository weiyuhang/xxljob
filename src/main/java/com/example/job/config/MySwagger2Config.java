package com.example.job.config;

import com.zyl.datasource.config.Swagger2Config;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author 13345
 * @create 2019-04-16 14:11
 * @desc
 **/
@Configuration
@EnableSwagger2
@Profile({"dev","test"})
public class MySwagger2Config extends Swagger2Config {

}
