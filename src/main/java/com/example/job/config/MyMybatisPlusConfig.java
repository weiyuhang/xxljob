package com.example.job.config;

import com.zyl.datasource.config.MybatisPlusConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author 13345
 * @create 2019-04-15 18:53
 * @desc
 **/
@Configuration
@MapperScan(basePackages = "com.example.job.mapper")
public class MyMybatisPlusConfig extends MybatisPlusConfig {

}
