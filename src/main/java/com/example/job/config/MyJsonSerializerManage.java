package com.example.job.config;


import com.zyl.datasource.config.JsonSerializerManage;
import org.springframework.boot.jackson.JsonComponent;

/**
* @author 13345
* @create 2019-04-15 18:53
* @desc
**/
@JsonComponent
public class MyJsonSerializerManage extends JsonSerializerManage {

}
