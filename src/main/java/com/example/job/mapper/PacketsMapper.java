package com.example.job.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.job.im.packets.entity.Packets;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 红包表 Mapper 接口
 * </p>
 *
 * @author weiyuhang
 * @since 2019-06-25
 */
@Mapper
public interface PacketsMapper extends BaseMapper<Packets> {

    /**
     * 收到红包列表
     * @param paramMap
     * @return
     */
    public List<Map> getGetPacketsList(Map paramMap);

    /**
     * 发出红包列表
     * @param paramMap
     * @return
     */
    public List<Map> getRecPacketsList(Map paramMap);


    /**
     * 收到红包总数
     * @param paramMap
     * @return
     */
    public Integer queryGetPacketsAllMountByUser(Map paramMap);

    /**
     * 发出红包总数
     * @param paramMap
     * @return
     */
    public Integer queryRecPacketsAllMountByUser(Map paramMap);
}
