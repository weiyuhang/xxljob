package com.example.job.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.job.user.zylBillDetail.entity.ZylBillDetail;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 金币流水表 Mapper 接口
 * </p>
 *
 * @author weiyuhang
 * @since 2019-08-30
 */
@Mapper
public interface ZylBillDetailMapper extends BaseMapper<ZylBillDetail> {

}
