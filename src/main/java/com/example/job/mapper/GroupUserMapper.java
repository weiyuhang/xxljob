package com.example.job.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.job.im.groupUser.entity.GroupUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 群组成员表 Mapper 接口
 * </p>
 *
 * @author weiyuhang
 * @since 2019-06-25
 */
@Mapper
public interface GroupUserMapper extends BaseMapper<GroupUser> {

    /**
     *
     * @return
     */
    public List<Map<String,Object>> countGroupUser(Map map);

}
