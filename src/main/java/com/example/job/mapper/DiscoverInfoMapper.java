package com.example.job.mapper;

import com.example.job.discover.entity.DiscoverMaxHot;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface DiscoverInfoMapper {



    List<DiscoverMaxHot> selectMaxHotByCollege(@Param("collegeId") String collegeId,
                                               @Param("cityId") String cityId,
                                               @Param("type") String type);



}
