package com.example.job.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.job.im.group.entity.Group;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 群聊表 Mapper 接口
 * </p>
 *
 * @author weiyuhang
 * @since 2019-06-25
 */
@Mapper
public interface GroupMapper extends BaseMapper<Group> {

    /**
     * 查询吐槽群列表
     * @param map
     * @return
     */
    List<Map<String,Object>> otherGroupListByUserId(Map<String, Object> map);
    /**
     * 查询所有校内学校
     * @param map
     * @return
     */
    List<Long> collegeGroupByList(Map<String, Object> map);
    /**
     * 查询所有同城城市
     * @param map
     * @return
     */
    List<Long> cityGroupByList(Map<String, Object> map);
    /**
     * 查询所有国家
     * @param map
     * @return
     */
    List<Long>  nationGroupByList(Map<String, Object> map);

}
