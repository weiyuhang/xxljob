package com.example.job.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.job.user.zylBillExt.entity.ZylBillExt;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 金币每月收支统计表 Mapper 接口
 * </p>
 *
 * @author weiyuhang
 * @since 2019-08-30
 */
@Mapper
public interface ZylBillExtMapper extends BaseMapper<ZylBillExt> {

}
