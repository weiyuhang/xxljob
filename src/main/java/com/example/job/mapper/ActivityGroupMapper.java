package com.example.job.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.job.im.activityGroup.entity.ActivityGroup;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author weiyuhang
 * @since 2019-08-09
 */
@Mapper
public interface ActivityGroupMapper extends BaseMapper<ActivityGroup> {

    public List<Map<String,String>> countGroupMessage(Map<String, Object> paramMap);

}
