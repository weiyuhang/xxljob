package com.example.job.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.job.discover.entity.DiscoverUser;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author magj
 * @since 2019-08-05
 */
@Mapper
public interface DiscoverUserMapper  {


    List<String> queryAllCollege();

    List<String> queryAllCity();

}
