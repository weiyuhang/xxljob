package com.example.job.exception;

public class BusiException extends RuntimeException  {



    private String code;
    private String msg;
    /**
     * 系统级异常，统一捕获
     */
    public BusiException(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
