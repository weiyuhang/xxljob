package com.example.job.im.group.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * <p>
 * 用户入参对象
 * </p>
 *
 * @author weiyuhang
 * @since 2019-06-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="UserParam对象，用户入参对象", description="")
public class UserParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id" ,required = true)
    private Long id ;

    @ApiModelProperty(value = "昵称")
    private String nickName  ;

    @ApiModelProperty(value = "头像链接")
    private String portraitUri ;

    @ApiModelProperty(value = "城市ID" )
    private Long cityId ;

    @ApiModelProperty(value = "学校ID")
    private Long collegeId ;

    @ApiModelProperty(value = "认证状态 0未认证 1认证蓝V 2认证红V 3认证失败")
    private Integer statues ;

}
