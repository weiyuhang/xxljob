package com.example.job.im.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.job.exception.BusiException;
import com.example.job.im.group.entity.Group;
import com.example.job.im.group.entity.GroupParam;
import com.zyl.core.Vo.ResultVo;

/**
 * <p>
 * 群聊表 服务类
 * </p>
 *
 * @author weiyuhang
 * @since 2019-06-25
 */
public interface IGroupService extends IService<Group> {

    /**
     * 解散群组
     * @param groupParam
     * @return
     */
    ResultVo dismissGroup(GroupParam groupParam) throws BusiException,Exception;


}
