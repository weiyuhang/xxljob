package com.example.job.im.group.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 群聊表
 * </p>
 *
 * @author weiyuhang
 * @since 2019-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_group")
@ApiModel(value="Group对象", description="群聊表")
public class Group implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "群组ID")
    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long id;

    @ApiModelProperty(value = "群组名称")
    private String name;

    @ApiModelProperty(value = "群组头像")
    private String portraitUri;

    @ApiModelProperty(value = "群组类型")
    private Integer groupType;

    @ApiModelProperty(value = "群公告")
    private String announcement;

    @ApiModelProperty(value = "群组状态(0任何人可进，1需管理员审核可进)")
    private Integer openStatus;

    @ApiModelProperty(value = "校内和同城标示（0校内1同城2校内，同城3全国4校内，全国5同城，全国6校内，同城，全国）")
    private Integer attribute;

    @ApiModelProperty(value = "城市ID ")
    private Long cityId;

    @ApiModelProperty(value = "城市名称 ")
    private String cityName;

    @ApiModelProperty(value = "学校ID ")
    private Long collegeId;

    @ApiModelProperty(value = "学校名称 ")
    private String collegeName;

    @ApiModelProperty(value = "国家ID ")
    private Long nationId;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;


}
