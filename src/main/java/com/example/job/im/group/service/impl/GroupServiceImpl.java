package com.example.job.im.group.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.job.exception.BusiException;
import com.example.job.im.group.entity.Group;
import com.example.job.im.group.entity.GroupParam;
import com.example.job.im.group.service.IGroupService;
import com.example.job.im.groupUser.entity.GroupUser;
import com.example.job.im.groupUser.service.IGroupUserService;
import com.example.job.mapper.GroupMapper;
import com.example.job.util.RongCloutHttpRequestUtil;
import com.zyl.core.Vo.ResultVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * <p>
 * 群聊表 服务实现类
 * </p>
 *
 * @author weiyuhang
 * @since 2019-06-25
 */
@Service
@Slf4j
public class GroupServiceImpl extends ServiceImpl<GroupMapper, Group> implements IGroupService {

    private final static String midMethod = "group/";

    @Resource
    private RongCloutHttpRequestUtil rongCloutHttpRequest;
    @Resource
    private IGroupUserService groupUserService;



    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public ResultVo dismissGroup(GroupParam groupParam) throws BusiException {
        if (null == groupParam.getId()) {
            return ResultVo.paramsError("群号不能为空");
        }
        if (null == groupParam.getUserParam()) {
            return ResultVo.paramsError("用户不能为空");
        }

        QueryWrapper<GroupUser> groupUserQueryWrapper = new QueryWrapper<>();
        groupUserQueryWrapper.eq("group_id", groupParam.getId());
        List<GroupUser> groupUserList = groupUserService.list(groupUserQueryWrapper);
        Map<String, Object> map = quitGroup(groupUserList, groupParam.getId());
        if (!map.isEmpty() && "200".equals(map.get("code").toString())) {
            groupUserService.remove(groupUserQueryWrapper);
        } else {
            return ResultVo.failResult();
        }
        String method = midMethod + "dismiss";
        StringBuilder sb = new StringBuilder();
        sb.append("userId=").append(groupParam.getUserParam().getId())
                .append("&groupId=").append(groupParam.getId());
        String body = sb.toString();
        Map<String, Object> resultMap = rongCloutHttpRequest.doPost(method, body);
        if (!resultMap.isEmpty() && "200".equals(resultMap.get("code").toString())) {
            this.removeById(groupParam.getId());
            return ResultVo.successResult();
        } else {
            return ResultVo.failResult();
        }
    }

    private Map<String, Object> quitGroup(List<GroupUser> groupUserList, Long groupId) {
        String method = midMethod +  "quit";
        StringBuilder sb = new StringBuilder();
        for (GroupUser groupUser : groupUserList) {
            sb.append("&userId=").append(groupUser.getUserId().toString());
        }
        sb.append("&groupId=").append(groupId);
        String body = sb.toString();
        if (body.indexOf("&") == 0) {
            body = body.substring(1, body.length());
        }
        Map<String, Object> resultMap = rongCloutHttpRequest.doPost(method, body);
        return resultMap;

    }

}
