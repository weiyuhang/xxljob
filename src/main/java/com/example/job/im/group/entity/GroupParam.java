package com.example.job.im.group.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 群聊表
 * </p>
 *
 * @author weiyuhang
 * @since 2019-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="GroupParam对象,群入参对象", description="")
public class GroupParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "当前用户",notes = "存放当前用户信息",required = true)
    private UserParam userParam;

    @ApiModelProperty(value = "群组ID" )
    private Long id;

    @ApiModelProperty(value = "群组名称")
    private String name;

    @ApiModelProperty(value = "群组头像")
    private String portraitUri;

    @ApiModelProperty(value = "群公告")
    private String announcement;

    @ApiModelProperty(value = "群组状态(0任何人可进，1需管理员审核可进)")
    private Integer openStatus;

    @ApiModelProperty(value = "校内和同城标示（0校内1同城2我全都要）")
    private Integer attribute;

    @ApiModelProperty(value = "入群理由")
    private String joinReason;

    @ApiModelProperty(value = "每页显示数量")
    private Integer pageSize;

    @ApiModelProperty(value = "当前页")
    private Integer current;


}
