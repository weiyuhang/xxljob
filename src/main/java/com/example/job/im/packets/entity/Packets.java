package com.example.job.im.packets.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 红包表
 * </p>
 *
 * @author weiyuhang
 * @since 2019-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_packets")
@ApiModel(value="Packets对象", description="红包表")
public class Packets implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "红包ID")
    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long id;

    @ApiModelProperty(value = "是否群聊红包（0是1不是）默认1")
    private Integer isGroup;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    @ApiModelProperty(value = "发红包id")
    private Long recUserId;

    @ApiModelProperty(value = "红包个数")
    private Integer packetsCount;

    @ApiModelProperty(value = "发给用户（群）的id")
    private Long getId;

    @ApiModelProperty(value = "单个红包金额")
    private Integer amount;

    @ApiModelProperty(value = "红包库存（为0时收完啦）")
    private Integer stock;

    @ApiModelProperty(value = "红包状态（0正常1已过期）")
    private Integer status;

    @ApiModelProperty(value = "内容")
    private String content;


}
