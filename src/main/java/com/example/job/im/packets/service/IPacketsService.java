package com.example.job.im.packets.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.job.im.packets.entity.Packets;

/**
 * <p>
 * 红包表 服务类
 * </p>
 *
 * @author weiyuhang
 * @since 2019-06-25
 */
public interface IPacketsService extends IService<Packets> {


}
