package com.example.job.im.packets.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.job.mapper.PacketsMapper;
import com.example.job.im.packets.entity.Packets;
import com.example.job.im.packets.service.IPacketsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


/**
 * <p>
 * 红包表 服务实现类
 * </p>
 *
 * @author weiyuhang
 * @since 2019-06-25
 */
@Slf4j
@Service
public class PacketsServiceImpl extends ServiceImpl<PacketsMapper, Packets> implements IPacketsService {



}
