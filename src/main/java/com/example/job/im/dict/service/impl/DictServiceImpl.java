package com.example.job.im.dict.service.impl;

import com.example.job.im.dict.entity.Dict;
import com.example.job.im.dict.service.IDictService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weiyuhang
 * @since 2019-07-10
 */
@Service
@Slf4j
public class DictServiceImpl  implements IDictService {

    @Resource
    private MongoTemplate mongoTemplate;
//    private static final Map<String, Object> codeMap = new ConcurrentHashMap<>();
//    @PostConstruct
//    public void init(){
//        log.info("加载字典表缓存开始======================");
//        Map<String, Object> map =loadCache();
//        log.info(JSONObject.toJSONString(map));
//        CacheLoader<String, Object> loader = new CacheLoader<String, Object>();
//        loader.loadCache(map, codeMap);
//
//    }

    public String getCodeValue(String name ) {
        Query dictQuery = new Query(Criteria.where("status").is(0));

        List<Dict> dictList = mongoTemplate.find(dictQuery, Dict.class);
        for(Dict dict :dictList){
            if(dict.getName().equals(name)){
                return dict.getCode();
            }
        }
        return null;
    }

//    /**
//     *
//     * @param appkey
//     * @return
//     */
//    public Object getCodeValue(String appkey) {
//        return codeMap.get(appkey);
//    }
}
