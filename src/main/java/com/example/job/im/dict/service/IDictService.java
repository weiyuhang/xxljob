package com.example.job.im.dict.service;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weiyuhang
 * @since 2019-07-10
 */
public interface IDictService {

    /**
     * 获取缓存值
     * @param key
     * @return
     */
    String getCodeValue(String key);

}
