package com.example.job.im.dict.entity;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author weiyuhang
 * @since 2019-07-10
 */
@Data
@Document(collection = "dict")
public class Dict implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "配置名称")
    private String name;

    @ApiModelProperty(value = "配置项的值")
    private String code;

    @ApiModelProperty(value = "状态0正常，1失效")
    private Integer status;

    @ApiModelProperty(value = "备注")
    private String remark;

}
