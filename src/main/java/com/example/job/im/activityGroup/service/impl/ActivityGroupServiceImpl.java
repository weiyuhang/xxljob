package com.example.job.im.activityGroup.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.job.im.activityGroup.entity.ActivityGroup;
import com.example.job.mapper.ActivityGroupMapper;
import com.example.job.im.activityGroup.service.IActivityGroupService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weiyuhang
 * @since 2019-08-09
 */
@Service
public class ActivityGroupServiceImpl extends ServiceImpl<ActivityGroupMapper, ActivityGroup> implements IActivityGroupService {

}
