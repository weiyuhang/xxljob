package com.example.job.im.activityGroup.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author weiyuhang
 * @since 2019-08-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_activity_group")
@ApiModel(value="ActivityGroup对象", description="")
public class ActivityGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "群组ID")
    private Long groupId;

    @ApiModelProperty(value = "群组活跃度（前一天的群聊天数和群人数的相加）")
    private Integer activity;

    @ApiModelProperty(value = "热度时间")
    private Date activityDate;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;


}
