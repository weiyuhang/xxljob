package com.example.job.im.activityGroup.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.job.im.activityGroup.entity.ActivityGroup;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weiyuhang
 * @since 2019-08-09
 */
public interface IActivityGroupService extends IService<ActivityGroup> {

}
