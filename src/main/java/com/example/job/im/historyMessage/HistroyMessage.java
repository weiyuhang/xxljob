package com.example.job.im.historyMessage;

import java.util.Date;
import java.util.List;

/**
 * Auto-generated: 2019-08-07 13:58:48
 */
public class HistroyMessage {

    private String appId;
    private String fromUserId;
    private String targetId;
    private int targetType;
    private String GroupId;
    private String classname;
    public Content content;
    private Date dateTime;
    private long timestamp;
    private String msgUID;
    private String source;
    private List<String> groupUserIds;

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppId() {
        return appId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetType(int targetType) {
        this.targetType = targetType;
    }

    public int getTargetType() {
        return targetType;
    }

    public void setGroupId(String GroupId) {
        this.GroupId = GroupId;
    }

    public String getGroupId() {
        return GroupId;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getClassname() {
        return classname;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public Content getContent() {
        return content;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setMsgUID(String msgUID) {
        this.msgUID = msgUID;
    }

    public String getMsgUID() {
        return msgUID;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }

    public void setGroupUserIds(List<String> groupUserIds) {
        this.groupUserIds = groupUserIds;
    }

    public List<String> getGroupUserIds() {
        return groupUserIds;
    }

}
