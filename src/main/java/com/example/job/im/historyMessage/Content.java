package com.example.job.im.historyMessage;

/**
 * Auto-generated: 2019-08-07 13:58:48
 */
public class Content {

    private String content;
    public User user;
    private boolean isBurnAfterRead;
    private Long lastMessageSendTime;
    private String type;
    private int burnDuration;

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setIsBurnAfterRead(boolean isBurnAfterRead) {
        this.isBurnAfterRead = isBurnAfterRead;
    }

    public boolean getIsBurnAfterRead() {
        return isBurnAfterRead;
    }

    public void setBurnDuration(int burnDuration) {
        this.burnDuration = burnDuration;
    }

    public int getBurnDuration() {
        return burnDuration;
    }

    public boolean isBurnAfterRead() {
        return isBurnAfterRead;
    }

    public void setBurnAfterRead(boolean burnAfterRead) {
        isBurnAfterRead = burnAfterRead;
    }

    public Long getLastMessageSendTime() {
        return lastMessageSendTime;
    }

    public void setLastMessageSendTime(Long lastMessageSendTime) {
        this.lastMessageSendTime = lastMessageSendTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}