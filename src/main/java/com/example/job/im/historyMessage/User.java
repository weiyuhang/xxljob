package com.example.job.im.historyMessage;

/**
 * Auto-generated: 2019-08-07 13:58:48
 */
public class User {

    private String id;
    private String name;
    private String portrait;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public String getPortrait() {
        return portrait;
    }

}