package com.example.job.im.groupUser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.job.im.groupUser.entity.GroupUser;

/**
 * <p>
 * 群组成员表 服务类
 * </p>
 *
 * @author weiyuhang
 * @since 2019-06-25
 */
public interface IGroupUserService extends IService<GroupUser> {

}
