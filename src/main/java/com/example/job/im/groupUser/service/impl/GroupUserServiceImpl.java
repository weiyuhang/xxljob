package com.example.job.im.groupUser.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.job.im.groupUser.entity.GroupUser;
import com.example.job.im.groupUser.service.IGroupUserService;
import com.example.job.mapper.GroupUserMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 群组成员表 服务实现类
 * </p>
 *
 * @author weiyuhang
 * @since 2019-06-25
 */
@Service
public class GroupUserServiceImpl extends ServiceImpl<GroupUserMapper, GroupUser> implements IGroupUserService {

}
