package com.example.job.im.groupUser.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 群组成员表
 * </p>
 *
 * @author weiyuhang
 * @since 2019-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_group_user")
@ApiModel(value="GroupUser对象", description="群组成员表")
public class GroupUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long id;

    @ApiModelProperty(value = "用户ID")
    private Long userId;

    @ApiModelProperty(value = "用户群昵称")
    private String groupUserNickName;

    @ApiModelProperty(value = "群组ID")
    private Long groupId;

    @ApiModelProperty(value = "用户状态（0正常，1被禁言）")
    private Integer userStatus;

    @ApiModelProperty(value = "用户类型；1群主，2普通群成员")
    private Integer groupUserType;

    private Date createTime;

    private Date updateTime;


}
