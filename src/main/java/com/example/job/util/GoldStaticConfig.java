package com.example.job.util;

public interface GoldStaticConfig {

    /**
     * "交易类型 0商城 1悬赏 2投票 3发现 4课程表 5评论 6建群 7发红包 8抢红包 9退红包"
     */
    public interface DealType{

        public static final String HOT_DISCOVER = "3";

        public static final String CREATE_GROUP = "6";

        public static final String REC_RED_PACKETS = "7";

        public static final String GET_RED_PACKETS = "8";

        public static final String RETURN_RED_PACKETS = "9";
    }

    /**
     * 金币操作（0支出1收获）
     */
    public interface ConsumeOrGet{

        public static final String CONSUME = "0";
       
        public static final String GET = "1";
    }
}
