package com.example.job.util;

import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class RongCloutHttpRequestUtil {

    @Value("${rongCloud.appKey}")
    private String appKey;

    @Value("${rongCloud.appSecret}")
    private String appSecret;

    @Value("${rongCloud.url}")
    private String url;


    public static final String TIMESTAMP = "Timestamp";
    public static final String SIGNATURE = "Signature";
    public static final String NONCE = "Nonce";
    public static final String APPKEY = "App-Key";


    /**
     * 调用RongCloud
     * @param method
     * @param object
     */
    public Map<String,Object> doPost(String method,Object object) {
        String nonce = String.valueOf(Math.random() * 1000000);
        String timestamp = String.valueOf(System.currentTimeMillis() / 1000);
        StringBuilder sb = new StringBuilder();
        String signature = String.valueOf(sb.append(appSecret).append(nonce).append(timestamp));

        String methodUrl = url+method+".json";
        log.error("methodUrl==="+methodUrl+"param===="+JSON.toJSONString(object));

        Map<String,Object> map = new HashMap<>();
        try {
            String result ="";
            if(object instanceof Map){
                result = HttpRequest.post(methodUrl)
                        .header(NONCE, nonce)
                        .header(APPKEY, appKey)
                        .header(TIMESTAMP,timestamp)
                        .header(SIGNATURE, CodeUtil.hexSHA1(signature))//头信息，多个头信息多次调用此方法即可)
                        .form((Map) object).timeout(3000).execute().body();
            }else if(object instanceof String){
                result = HttpRequest.post(methodUrl)
                        .header(NONCE, nonce)
                        .header(APPKEY, appKey)
                        .header(TIMESTAMP,timestamp)
                        .header(SIGNATURE, CodeUtil.hexSHA1(signature))//头信息，多个头信息多次调用此方法即可)
                        .body((String)object).timeout(3000).execute().body();
            }else if ( null == object ){
                result = HttpRequest.post(methodUrl)
                        .header(NONCE, nonce)
                        .header(APPKEY, appKey)
                        .header(TIMESTAMP,timestamp)
                        .header(SIGNATURE, CodeUtil.hexSHA1(signature))//头信息，多个头信息多次调用此方法即可)
                        .timeout(3000).execute().body();
            }
            map = (Map<String,Object> )JSON.parse(result);
        }catch (RuntimeException e){
            log.error(e.getMessage());
        }

        return map;
    }

}
