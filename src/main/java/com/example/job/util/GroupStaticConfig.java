package com.example.job.util;

/**
 * 群
 */
public interface GroupStaticConfig {

    /**
     * 群组类型 0
     */
    public interface GroupType{
        public static final String NORMAL = "0";
    }

    /**
     * 用户类型；1群主，2普通群成员 group_user_type
     */
    public interface GroupUserType{
        public static final String GROUP_CREATOR = "1";
        public static final String GROUP_MEMBER = "2";
    }

    /**
     * 校内和同城标示（0校内1同城）attribute
     */
    public interface Attribute{
        public static final String SAME_UNIVERSITY = "0";
        public static final String SAME_CITY = "1";
        public static final String BOTH = "2";
        public static final String SAME_NATION = "3";
    }

    /**
     * 群组状态(0任何人可进，1需管理员审核可进) open_status
     */
    public interface OpenStatus{
        public static final String ALL_OPEN = "0";
        public static final String AUDIT_OPEN = "1";
    }
    /**
     * 用户状态（0正常，1被禁言）user_status
     */
    public interface UserStatus{
        public static final String NO_SILENCED  = "0";
        public static final String SILENCED = "1";
    }
    /**
     * 入群是否审核(0待审核，1审核通过，2驳回) is_audit
     */
    public interface IsAudit{
        public static final String WAIT_AUDIT = "0";
        public static final String PASS_AUDIT = "1";
        public static final String REJECT_AUDIT = "2";
    }
}
